const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userSchema = Schema({
    userName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    passwordUpdateTime: {
        type: Date,
        required: true
    },
    birthday: {
        type:String,
        default:""

    },
    phone: {
        type: String,
        required: true
    },
    picture_url:{
        type:String,
        default:""
    }
})
module.exports=mongoose.model("users",userSchema);