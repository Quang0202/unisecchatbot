const express = require("express");
const Router = express.Router();
const usersControllers= require("../controllers/users.controllers");

Router.post("/registration", usersControllers.SignUp);
Router.post("/login", usersControllers.SignIn);

module.exports = Router;