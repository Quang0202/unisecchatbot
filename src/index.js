const app = require("./ExpressBundle.js");
const config = require("config");
const userRouters= require("./routes/users.routes");
const port = process.env.PORT || config.get("PORT");
app.get("/", (req, res) => {
    res.send("Hello. I'm Unisec chatbot")
});
app.use("/api/v1/users",userRouters);
app.listen(port, () => {
    console.log("server is running on port: ", port);
});

module.exports = app;