const userModels = require("../models/users.models");
const responseFactory = require("response-factory");
const bcrypt = require("bcrypt");
const config = require("config");
const mongoose = require("mongoose");
const accountHelper = require("../helpers/accounts.helper");
const { pass } = require("../database");
const { verifyAccessToken } = require("../middlewares/tokens.middlewares");

const SignUp = async (req, res) => {
    try {
        const { userName, email, password, phone } = req.body;
        if (!userName
            || !email
            || !password
            || !phone) {
            throw new Error("Misssing argument")
        }
        const PasswordValidate = accountHelper.validatePass(password);
        if (!PasswordValidate.valid) {
            throw new Error(PasswordValidate.message);
        }
        const usersWithInformation = await userModels.find({
            $or: [
                { email },
                { phone }
            ]
        });
        if (usersWithInformation.length) {
            let message = "đã tồn tại";
            const users = usersWithInformation[0];
            message = users.email === email ? "Email " + message : message;
            message = users.phone === phone ? "Số điện thoại, " + message : message;
            throw new Error(message);
        }
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(password, salt);
        const nowTimeStamp = Date.now();
        const nowDateType = new Date(nowTimeStamp);
        const user = new userModels({
            userName,
            email,
            password: hashPassword,
            passwordUpdateTime: nowDateType,
            phone
        })
        user.save();
        const success = responseFactory.success({ data: {} });
        res.json(success);

    } catch (err) {
        const fail = responseFactory.fail({ reason: err.message });
        res.json(fail);
    }
}
const SignIn = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!email
            || !password) {
            throw new Error("Missing argument");
        }
        const userWithEmail = await userModels.find({ email });
        if (!userWithEmail.length) {
            throw new Error("Tài khoản không tồn tại");
        }
        const { _id, passwordUpdateTime } = userWithEmail[0];
        const checkPass = bcrypt.compareSync(password, userWithEmail[0].password);
        if (!checkPass) {
            throw new Error("Mật khẩu không chính xác");
        }
        const tokenPayload = {
            id: _id,
            email,
            passwordUpdateTime
        }
        const accessToken = accountHelper.generateToken(tokenPayload,
            config.get("ACCESS_TOKEN_EXPIRE_TIME"),
            config.get("SECRET_ACCESS_TOKEN_KEY"));
        const success = responseFactory.success({data:{accessToken}});
        res.json(success);
    } catch (err) {
        const fail = responseFactory.fail({ reason: err.message });
        res.json(fail);
    }
}
module.exports = {
    SignUp,
    SignIn
}