const jwt = require("jsonwebtoken");
const config = require("config");
const secretTokenKey = config.get("SECRET_ACCESS_TOKEN_KEY");
const responseFactory= require("response-factory");

function verifyAccessToken(req,res,next) {
    const {headers:{authorization}}=req;
    const Regx = /\s+(.*)/g;
    const token = Regx.exec(authorization)[1];
    jwt.verify(token,secretTokenKey, async(err , decoded)=>{
        if (err) {
            return res.json(
                responseFactory
                    .authenticationFail({
                        reason: "User is unauthorized"
                    })
            )
        }
        req.user = await UsersModel.findById(decoded.id);
        const passwordTokenUpdateTime = new Date(decoded.passwordUpdateTime);
        const passwordUserUpdateTime = new Date(req.user.passwordUpdateTime);
        if(passwordTokenUpdateTime < passwordUserUpdateTime) {
            return res.json(
                responseFactory
                    .authenticationFail({
                        reason: "User is unauthorized"
                    })
            )
        }
        req.token = decoded;
        next();
    } )
}
module.exports ={
    verifyAccessToken
}